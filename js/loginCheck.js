var xmlHttp = new XMLHttpRequest();

/*---------------------------------------------------*/

function LoginStateChange()
{
  const doneState = 4;
  const correctResponse = 200;

  if (xmlHttp.readyState == doneState)
  {
      var status = xmlHttp.status;

      if (status == correctResponse) LoginResponseRender(xmlHttp.responseText)
      else console.log("Ответ сервера " + xmlHttp.statusText);
  }
}

/*---------------------------------------------------*/

function LoginResponseRender(loginResponse)
{
  const successLogin = "Success!";

  if (loginResponse === successLogin) LoginResponseRenderSuccess();
  else LoginResponseRenderFail(loginResponse);
}

/*---------------------------------------------------*/

function LoginResponseRenderSuccess()
{
  const successLogin = "Success!";
  let loginInput = document.querySelector("input[name = 'email']");
  let loginMessage = document.querySelector("input[name = 'email'] + p");

  loginInput.className = "successInput";

  let message = document.createElement("p");
  message.textContent = successLogin;

  let parent = loginInput.parentNode;
  if (loginMessage) parent.replaceChild(message, loginMessage);
  else parent.appendChild(message);
}

/*---------------------------------------------------*/

function LoginResponseRenderFail(failResponse)
{
  let loginInput = document.querySelector("input[name = 'email']");
  let loginMessage = document.querySelector("input[name = 'email'] + p");

  loginInput.className = "failedInput";

  let message = document.createElement("p");
  message.textContent = failResponse;

  let parent = loginInput.parentNode;
  if (loginMessage) parent.replaceChild(message, loginMessage);
  else parent.appendChild(message);
}


/*---------------------------------------------------*/

document.addEventListener("DOMContentLoaded", function()
{
  let loginInput = document.querySelector("input[name = 'email']");

  loginInput.addEventListener("blur", function()
  {
    let data = loginInput.value;
    let url = `/web/signup/check?email=${data}`;    //Sending on SignupController checking

    xmlHttp.open("GET", url, true)
    xmlHttp.setRequestHeader("Content-Type", "text/plain");
    xmlHttp.onreadystatechange = LoginStateChange;
    xmlHttp.send();
  })
});
