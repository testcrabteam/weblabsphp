var currentBlogId;

/*---------------------------------------------------*/

function BindFormBoxEvents()
{
  let closeFormsButton = document.querySelector("#commentBox .closeFormButton");

  closeFormsButton.addEventListener("click", CloseFormBox);

  let submitButton = document.querySelector("#commentBox .commentSubmit");

  submitButton.addEventListener("click", function(event)
  {
    event.preventDefault();
    SubmitComment();
  });
}

function CloseFormBox()
{
  let commentBox = document.getElementById("commentBox");
  commentBox.style.display = "none";
}

/*---------------------------------------------------*/

function SubmitComment()
{
  let comment = {};
  let textarea = document.querySelector("#commentBox textarea");

  comment.postId = currentBlogId;
  comment.commentText = textarea.value;
  console.log(comment);

  let response = fetch('/web/comment/save',
  {
    method: 'POST',
    headers: {
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
    },
    body: `postId=${currentBlogId}&commentText=${textarea.value}`
  });

  response.then(function(response)
  {
    console.log(response);
    if (response.status !== 200)
    {
      console.log('Looks like there was a problem. Status Code: ' +
        response.status);
      return;
    }

    // Examine the text in the response
    response.json().then(function(data)
    {
      console.log(data);
      CloseFormBox();
      AddNewComment(data);
    });
  })
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });
}

/*---------------------------------------------------*/

function AddNewComment(commentObj)
{
  let postIdName = "button" + commentObj.postid;
  let postButton = document.getElementById(postIdName);

  //Button->form-buttons->text???->commentsBlock
  let commentsBlock = postButton.parentNode.previousSibling.previousSibling;

  let htmlComment = GetHtmlComment(commentObj);
  commentsBlock.appendChild(htmlComment);
}

/*---------------------------------------------------*/

function GetHtmlComment(commentObj)
{
  //Create parent block
  let commentBlock = document.createElement("div");
  commentBlock.className = "userComment";

  //Create comment name block
  let commentName = document.createElement("h3");
  commentName.textContent = commentObj.name;

  //Create comment date block
  let commentDate = document.createElement("span");
  commentDate.textContent = commentObj.date;

  //Create comment text block
  let commentText = document.createElement("p");
  commentText.textContent = commentObj.text;

  //Attach to parent
  commentBlock.appendChild(commentName);
  commentBlock.appendChild(commentDate);
  commentBlock.appendChild(commentText);

  return commentBlock;
}

/*---------------------------------------------------*/

function BindCommentEvents()
{
  let commentButtons = document.querySelectorAll(".commentButton");

  commentButtons.forEach(function(button)
  {
    button.addEventListener("click", FormBoxRender);
    button.addEventListener("click", DetermineBlogId)
  });

}

/*---------------------------------------------------*/

function FormBoxRender()
{
  let commentBox = document.getElementById("commentBox");
  commentBox.style.display = "block";
}

/*---------------------------------------------------*/

function DetermineBlogId(event)
{
  let button = event.target;
  let buttonId = button.getAttribute("id");
  let postId = buttonId.split("button")[1];

  currentBlogId = postId;
}


/*---------------------------------------------------*/

document.addEventListener("DOMContentLoaded", function()
{
  BindCommentEvents();
  BindFormBoxEvents();
});
