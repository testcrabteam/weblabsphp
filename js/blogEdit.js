var currentBlogId;

/*---------------------------------------------------*/

function BindFormBoxEvents()
{
  let closeFormsButton = document.querySelector("#blogEditBox .closeFormButton");

  closeFormsButton.addEventListener("click", CloseFormBox);

  let submitButton = document.querySelector("#blogEditBox .blogEditSubmit");

  submitButton.addEventListener("click", function(event)
  {
    event.preventDefault();
    SubmitBlog();
  });
}

function CloseFormBox()
{
  let blogEditBox = document.getElementById("blogEditBox");
  blogEditBox.style.display = "none";
}

/*---------------------------------------------------*/

function SubmitBlog()
{
  let myform = document.blogEditForm;
  myform.blogId.value = currentBlogId;

  //создание iFrame
	iframe = document.createElement('iframe');
  iframe.name = myform.target= Date.parse(new Date);
  iframe.style.display = 'none';

  iframe.onload = iframe.onreadystatechange = function()
  {
    let window = iframe.contentWindow;
    let body = window.document.body;
    let responseStr = body.innerHTML;

    if (responseStr != "")
    {
      let responseObj = JSON.parse(responseStr);
      UpdateBlogByObj(responseObj);
      CloseFormBox();
    }
  }

  document.body.appendChild(iframe);
  myform.submit();
}

/*---------------------------------------------------*/

function UpdateBlogByObj(blogObj)
{
  let blogIdName = "button" + blogObj.id;
  let blogButton = document.getElementById(blogIdName);

  //Button->form-buttons->blogPost
  let blogPost = blogButton.parentNode.parentNode;
  
  UpdateBlogContent(blogPost, blogObj);
}

/*---------------------------------------------------*/

function UpdateBlogContent(blogPost, blogObj)
{
  let blogName = blogPost.querySelector("h1");
  let blogText = blogPost.querySelector("p");

  blogName.textContent = blogObj.theme;
  blogText.textContent = blogObj.content;
}

/*---------------------------------------------------*/

function BindCommentEvents()
{
  let blogEditButtons = document.querySelectorAll(".blogEditButton");

  blogEditButtons.forEach(function(button)
  {
    button.addEventListener("click", FormBoxRender);
    button.addEventListener("click", DetermineBlogId)
  });

}

/*---------------------------------------------------*/

function FormBoxRender()
{
  let blogEditBox = document.getElementById("blogEditBox");
  blogEditBox.style.display = "block";
}

/*---------------------------------------------------*/

function DetermineBlogId(event)
{
  let button = event.target;
  let buttonId = button.getAttribute("id");
  let postId = buttonId.split("button")[1];

  currentBlogId = postId;
}


/*---------------------------------------------------*/

document.addEventListener("DOMContentLoaded", function()
{
  BindCommentEvents();
  BindFormBoxEvents();
});
