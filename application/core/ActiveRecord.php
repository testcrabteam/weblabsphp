<?php

  class BaseActiveRecord
  {

    public static $pdo;     //PDO connection

    protected static $tableName;
    protected static $dbFields = array();

    protected static $idName; //For unique field using

    public function __construct()
    {
      if (!static::$tableName) return ;

      static::SetupConnection();
      $this->GetFields();
    }

    /*---------------------------------------------------*/

    public static function GetFields()
    {
      $stmt = static::$pdo->query("SHOW FIELDS FROM ".static::$tableName);

      //Save table fields
      while ($row = $stmt->fetch())
      {
        static::$dbFields[$row['Field']] = $row['Type'];
      }
    }

    /*---------------------------------------------------*/

    public static function SetupConnection()
    {
      if (!isset(static::$pdo))
      {
        $eror = false;
        try
        {
          $dsn = "mysql:dbname=studyweb; host=localhost; charset=utf8";
          $userName = "root";
          $pswd = "";

          static::$pdo = new PDO($dsn, $userName, $pswd);
        }
        catch (PDOException $ex)
        {
          die("Ошибка подключения к БД: $ex");
        }
      }
    }

    /*---------------------------------------------------*/

    public static function Find($id)
    {
      $idName = static::$idName;
      $id = static::ConvertFieldValueToString($idName, $id);

      $sql = "SELECT * FROM ".static::$tableName." WHERE $idName = $id";
      $stmt = static::$pdo->query($sql);

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      if (!$row) return null;

      $ar_obj = new static();
      foreach ($row as $key => $value)
      {
        $ar_obj ->$key = $value;
      }

      return $ar_obj;
    }

    /*---------------------------------------------------*/

    protected static function ConvertFieldValueToString($fieldName, $value)
    {
      $fieldType = static::$dbFields[$fieldName];

      //To string
      if (strpos($fieldType,'int')===false) $value = "'$value'";

      return $value;
    }

    /*---------------------------------------------------*/

    public static function FindAll()
    {
      $sql = "SELECT * FROM ".static::$tableName." WHERE 1";
      $stmt = static::$pdo->query($sql);
      $resultArr = array();

      //Save table records
      while ($row = $stmt->fetch())
      {
        $ar_obj = new static();

        //Save current table record
        foreach ($row as $key => $value)
        {
          if (is_string($key))
            $ar_obj ->$key = $value;
        }

        array_push($resultArr, $ar_obj);
      }

      return $resultArr;
    }

    /*---------------------------------------------------*/

    public function Save()
    {
      $fieldsList = static::GetFieldsList();
      $setComponentQuery = " SET ".join(', ',$fieldsList);
      $idName = static::$idName;

      if (static::Find($this->$idName))
      {
        $sql = "UPDATE ".static::$tableName.$setComponentQuery." WHERE ". $idName . " = " . $this->$idName;
      }
      else
      {
        $sql = "INSERT INTO ".static::$tableName.$setComponentQuery;
      }

      //Make query
      static::$pdo->query($sql);
    }

    /*---------------------------------------------------*/

    public function Delete()
    {
      $sql = "DELETE FROM ".static::$tableName." WHERE ID=".$this->id;
      $stmt = static::$pdo->query($sql);

      if($stmt) return $stmt->fetch(PDO::FETCH_ASSOC);
      else print_r(static::$pdo->errorInfo());
    }

    /*---------------------------------------------------*/

    protected function GetFieldsList()
    {
      $fieldsList = array();

      foreach (static::$dbFields as $field => $field_type)
      {
        if (isset($this->$field))
        {
          $value = $this->$field;
          $value = static::ConvertFieldValueToString($field, $value);

          //Array a=b, b=c, etc.
          $fieldsList[] = "$field = $value";
        }
      }

      return $fieldsList;
    }
  }


?>
