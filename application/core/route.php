<?php

  class Route
  {
    public static function Start()
    {
      require_once "application/models/records/UsersActiveRecord.php";
      session_start();

      $urlNoGet = explode("?", $_SERVER["REQUEST_URI"]);
      $routes = explode("/", $urlNoGet[0]);

      $controllerName = "page";
      $actionName = "index";
      $accessMode = "user";

      if (!empty($routes[2])) $controllerName = $routes[2];
      if (!empty($routes[3])) $actionName = $routes[3];
      if (!empty($routes[4])) $accessMode = $routes[4];
      if ($controllerName === "getadmin") $accessMode = "admin";

      $actionName = "Action" . ucfirst($actionName);

      $modelName = $controllerName.'Model';

      //Detertmine connection statement by access mode
      $userModePath = lcfirst($accessMode) . "/";
      $userModeFilePrefix = ucfirst($accessMode);
      $userModeClassPrefix = ucfirst($accessMode);

      //Connect abstract classes
      include_once "application/modes/${userModePath}controllers/" . $userModeFilePrefix . "Controller.php";
      include_once "application/modes/${userModePath}views/" . $userModeFilePrefix . "View.php";

      //Create Model, if that is
      $modelFile = $modelName.'.php';
      $modelPath = "application/models/".$modelFile;

      //Find model
      if(file_exists($modelPath)) include_once "application/models/".$modelFile;

      $controllerFile =
      "application/modes/${userModePath}controllers/" . $userModeFilePrefix . $controllerName . 'Controller.php';

      //Find controller file
      if (file_exists($controllerFile)) include_once $controllerFile;
      else echo $controllerFile;                                  //Route::ErrorPage404();

      //Controller obj
      $controllerClassName = $userModeClassPrefix . ucfirst($controllerName).'Controller';
      $controller = new $controllerClassName;

      //Controller action handling
      if(method_exists($controller, $actionName)) $controller->$actionName();
      else echo $actionName;                                   //Route::ErrorPage404();

  	}

  	function ErrorPage404()
  	{
      $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
      header('HTTP/1.1 404 Not Found');
  		header("Status: 404 Not Found");
  		header('Location:'.$host.'404');
    }
  }

?>
