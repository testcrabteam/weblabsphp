<?php

class UserTestsController extends UserController
{

  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("tests");
    $this->model = new TestsModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("UserTestsView.php", "UserLayoutView.php");
  }

  /*---------------------------------------------------*/

  private function RenderWithData($data)
  {
    $this->view->Render("UserTestsView.php", "UserLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSubmit()
  {
    $model = $this->model;
    $data = $model->Submit();              //Get validation data (errors)
    $this->RenderWithData($data);          //Update page
  }
}

?>
