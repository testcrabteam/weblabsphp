<?php

class UserStudyController extends UserController
{
  public function ActionIndex()
  {
    $this->SaveStatistics("study");
    $this->view->Render("UserStudyView.php", "UserLayoutView.php");
  }
}

?>
