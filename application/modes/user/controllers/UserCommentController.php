<?php

class UserCommentController extends UserController
{
  public function __construct()
  {
    parent::__construct();
    $this->model = new CommentModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    header("Location: /web/blog");
  }

  /*---------------------------------------------------*/

  public function ActionSave()
  {
    $record = $this->model->SaveComment();
    echo json_encode($record);
  }

}

?>
