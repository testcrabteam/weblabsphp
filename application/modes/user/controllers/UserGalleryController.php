<?php

class UserGalleryController extends UserController
{

  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("gallery");
    $this->model = new GalleryModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $data = $this->ActionGetImageData();
    $this->view->Render("UserGalleryView.php", "UserLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  private function ActionGetImageData()
  {
    $data = $this->model->GetData();
    return $data;
  }
}

?>
