<?php

class UserBlogController extends UserController
{
  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("blog");
    $this->model = new BlogModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $posts = $this->model->GetData();
    $postsElements = $posts[0];
    $comments = $posts[1];

    $data[0] = $postsElements;
    $data[1] = $comments;
    $data[2] = $this->model->GetPagesCount();
    $this->view->Render("UserBlogView.php", "UserLayoutView.php", $data);
  }
}

?>
