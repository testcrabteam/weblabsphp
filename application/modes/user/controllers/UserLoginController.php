<?php

class UserLoginController extends UserController
{

  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("login");
    $this->model = new LoginModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->UserExit();
    $this->view->Render("UserLoginView.php", "UserLayoutView.php");
  }

  /*---------------------------------------------------*/

  public function ActionSubmit()
  {
    $error = $this->model->LogIn();
    $this->view->Render("UserLoginView.php", "UserLayoutView.php", $error);
  }

  /*---------------------------------------------------*/

  private function UserExit()
  {
    if (isset($_SESSION["user"]))
    {
      session_destroy();
      header("Location: /web/");
    }
  }
}

?>
