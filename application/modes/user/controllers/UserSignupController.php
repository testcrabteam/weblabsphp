<?php

class UserSignupController extends UserController
{

  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("signup");
    $this->model = new SignupModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("UserSignupView.php", "UserLayoutView.php");
  }

  /*---------------------------------------------------*/

  public function ActionSubmit()
  {
    $model = $this->model;
    $data = $model->SignUp();              //Get validation data (errors)

    if ($data) $this->RenderWithErrors($data);
    else header('Location:/web/page/index/user');
  }

  /*---------------------------------------------------*/

  public function ActionCheck()
  {
    $model = $this->model;
    $data = $model->CheckUser();
    echo $data;
  }

  /*---------------------------------------------------*/

  private function RenderWithErrors($errors)
  {
    $this->view->Render("UserSignupView.php", "UserLayoutView.php", $errors);
  }
}

?>
