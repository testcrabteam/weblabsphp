<?php

class UserAboutController extends UserController
{
  public function ActionIndex()
  {
    $this->SaveStatistics("about");
    $this->view->Render("UserAboutView.php", "UserLayoutView.php");
  }
}

?>
