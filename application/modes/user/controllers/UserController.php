<?php
  require_once "application/models/StatisticsModel.php";

  class UserController extends Controller
  {
    private $statisticModel;

    public function __construct()
    {
      $this->view = new UserView();
      $this->statisticModel = new StatisticsModel();
    }

    /*---------------------------------------------------*/

    public function SaveStatistics($page)
    {
      $this->statisticModel->SaveStatistics($page);
    }

  }

?>
