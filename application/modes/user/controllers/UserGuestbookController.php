<?php

class UserGuestbookController extends UserController
{
  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("guestbook");
    $this->model = new GuestbookModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $data = $this->model->GetData();
    $this->view->Render("UserGuestbookView.php", "UserLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSend()
  {
    $this->model->SaveMessageFromPost();
    $this->ActionIndex();
  }
}

?>
