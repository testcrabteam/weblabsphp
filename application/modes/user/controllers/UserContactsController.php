<?php

class UserContactsController extends UserController
{

  public function __construct()
  {
    parent::__construct();
    $this->SaveStatistics("contacts");
    $this->model = new ContactsModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("UserContactsView.php", "UserLayoutView.php");
  }

  /*---------------------------------------------------*/

  private function RenderWithData($data)
  {
    $this->view->Render("UserContactsView.php", "UserLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSignUp()
  {
    $model = $this->model;
    $data = $model->SignUp();              //Get validation data (errors)
    $this->RenderWithData($data);          //Update page
  }
}

?>
