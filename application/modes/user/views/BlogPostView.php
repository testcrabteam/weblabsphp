<div class="blogPost">
  <h1><?php echo $postsArr[$i]->theme ?></h1>
  <span><?php echo $postsArr[$i]->date ?></span>
  <p><?php echo $postsArr[$i]->content ?></p>
  <img src=<?php echo "/web/images/user/".$postsArr[$i]->imagename ?> alt="">
  <?php

    if (isset($_SESSION["admin"]))
    {
      include "application/modes/user/views/BlogEditButtonView.php";
    }

   ?>
  <div class="blogComments">
    <h2>Comments:</h2>
    <?php

      foreach($commentsArr as $comment)
      {
        if ($comment->postid == $postsArr[$i]->id)
          include "application/modes/user/views/BlogCommentView.php";
      }

     ?>
  </div>
  <?php

    if (isset($_SESSION["user"]))
    {
      include "application/modes/user/views/CommentButtonView.php";
    }

  ?>
</div>
