<script type="text/javascript" src = "/web/js/blogComment.js"></script>

<div class="formBox" id = "commentBox">
  <div class="closeFormHeader">
    <div class="closeFormButton"></div>
  </div>
  <div class="formBoxContent">
    <form action="/web/comment/save" method = "POST" name = "commentForm" id = "commentForm">
      <div class="inputBlock">
        <h3>What do you think about this post?</h3>
        <textarea name="commentText" rows="8" cols="80" placeholder="Write your comment here..."></textarea>
      </div>
      <div class="form-buttons">
        <input type="submit" class = "helper-button commentSubmit" value = "Login" form = "commentForm">
      </div>
    </form>
  </div>
</div>

<div class = "queue center">
  <div class="limitation">
    <?php

      $postsArr = $data[0];
      $commentsArr = $data[1];
      
      for ($i = 0; $i < count($postsArr); $i++)
      {
        include "application/modes/user/views/BlogPostView.php";
      }

    ?>
    <div class="queueFooter">
      <?php

        $pagesCount = $data[2];
        for ($i = 1; $i <= $pagesCount; $i++)
        {
          include "application/modes/user/views/PageLinkView.php";
        }

       ?>
    </div>
  </div>
</div>
