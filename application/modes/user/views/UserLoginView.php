<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Come in!</h2>
      <p>We'll waiting for you!</p>
      <?php

        if ($data) echo "<p class = 'failedText'>${data}</p>"

       ?>
      <div class="spacer">
        <img src="/web/images/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form alone">
        <form action="/web/login/submit" method = "POST" name = "form" id = "form">
          <div class="inputBlock">
            <input type="text" placeholder = "Email" name = "email" autocomplete="off">
          </div>
           <div class="inputBlock">
             <input type="password" placeholder = "Password" name = "password" autocomplete="off">
           </div>
          <div class="form-buttons">
            <input type="submit" class = "helper-button" value = "Login">
            <a class = "helper-button" href = "/web/signup">
              <span>Sign Up</span>
            </a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
