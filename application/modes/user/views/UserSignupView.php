<script type="text/javascript" src = "/web/js/loginCheck.js"></script>
<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Welcome, my dear friend!</h2>
      <p>We have been waiting for you for a long time!</p>
      <?php

        if ($data) echo "<p class = 'failedText'>${data}</p>"

       ?>
      <div class="spacer">
        <img src="/web/images/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form alone">
        <form action="/web/signup/submit" method = "POST" name = "form" id = "form">
          <div class="inputBlock">
            <input type="text" placeholder = "Name" name = "name" autocomplete="off">
          </div>
          <div class="inputBlock">
            <input type="text" placeholder = "Email" name = "email" autocomplete="off">
          </div>
           <div class="inputBlock">
             <input type="password" placeholder = "Password" name = "password" autocomplete="off">
           </div>
          <div class="form-buttons">
            <input type="submit" class = "helper-button" value = "Submit">
            <input type="reset" class = "helper-button" value = "Reset">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
