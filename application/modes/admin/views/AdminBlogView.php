<script type="text/javascript" src = "/web/js/blogEdit.js"></script>

<div class="formBox" id = "blogEditBox">
  <div class="closeFormHeader">
    <div class="closeFormButton"></div>
  </div>
  <div class="formBoxContent">
    <form action="/web/blog/update/admin" method = "POST" name = "blogEditForm" id = "blogEditForm">
      <div class="inputBlock">
        <h3>Your name</h3>
        <input type="text" name="blogId">
        <input type="text" name="blogName" placeholder="Write your name here...">
      </div>
      <div class="inputBlock">
        <h3>Your description</h3>
        <textarea name="blogContent" rows="3" cols="80" placeholder="Write your content here..."></textarea>
      </div>
      <div class="form-buttons">
        <input type="submit" class = "helper-button blogEditSubmit" value = "Edit" form = "blogEditForm">
      </div>
    </form>
  </div>
</div>

<div class = "queue center">
  <div class="limitation">
    <?php

      $postsArr = $data[0];
      $commentsArr = $data[1];

      for ($i = 0; $i < count($postsArr); $i++)
      {
        include "application/modes/user/views/BlogPostView.php";
      }

    ?>
    <div class="queueFooter">
      <?php

        $pagesCount = $data[2];
        for ($i = 1; $i <= $pagesCount; $i++)
        {
          include "application/modes/user/views/PageLinkView.php";
        }

       ?>
    </div>
  </div>
</div>
