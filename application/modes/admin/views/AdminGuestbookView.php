
<div class = "queue center">
  <div class="limitation">
    <?php

      for ($i = 0; $i < count($data); $i++)
      {
        include "application/modes/user/views/MessageView.php";
      }

    ?>
  </div>
</div>
<div id="form">
        <div class="limitation">
          <div class="form-header">
            <h2>Feedback</h2>
            <p>You can send us your opinion!</p>
            <div class="spacer">
              <img src="/web/images/spacer.png" alt="spacer">
            </div>
          </div>
          <div class="feedback">
            <div class="feedback-form center">
              <form action="/web/guestbook/send" method = "POST" name = "form" id = "form">
                 <div class="inputBlock">
                   <input type="text" placeholder = "Your Name" name = "name" autocomplete="off">
                 </div>
                 <div class="inputBlock">
                   <input type="text" placeholder = "Email" name = "email" autocomplete="off">
                 </div>
                 <div class="inputBlock">
                   <input type="text" placeholder = "Your message" name = "messageText" autocomplete="off">
                 </div>
                <div class="spaceBetween">
                  <input type="submit" class = "helper-button" value = "Submit">
                  <input type="reset" class = "helper-button" value = "Reset">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
