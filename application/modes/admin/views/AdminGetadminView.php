<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Come in!</h2>
      <p>Site can't work without You!</p>
      <div class="spacer">
        <img src="/web/images/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form alone">
        <form action="/web/getadmin/submit" method = "POST" name = "loginForm" id = "loginForm">
          <div class="inputBlock">
            <input type="text"
              placeholder = "Login"
              name = "login"
              autocomplete="off"
              class =
                <?php if (!is_null($data[0])) echo "failedInput" ?>
            >
            <p><?php if (!is_null($data[0])) echo $data[0] ?></p>
          </div>
          <div class="inputBlock">
            <input type="password"
              placeholder = "Password"
              name = "password"
              autocomplete="off"
              class =
                <?php if (!is_null($data[1])) echo "failedInput" ?>
            >
            <p><?php if (!is_null($data[1])) echo $data[1] ?></p>
          </div>
           <div class="form-buttons">
             <input type="submit" class = "helper-button" value = "Submit">
             <input type="reset" class = "helper-button" value = "Reset">
           </div>
        </form>
      </div>
    </div>
  </div>
</div>
