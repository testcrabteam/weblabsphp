<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Web for Mister Class</title>
  <link rel="icon" href="/images/icon.png">
  <link rel="stylesheet" type = "text/css" href="/web/css/style.css">
  <script type = "text/javascript" src = "scripts/main.js"></script>
</head>
<body>
  <div id = "root feedback-main">
    <header>
      <div class="limitation">
        <div id="header-top">
          <div id="header-logo">
            <a href="index.html"><img src="/web/images/logo.png" alt = "Logo"></a>
              <div class="dateTime">
                <span>
                <?php

                  if (isset($_SESSION["admin"]))
                  {
                    $currentUser = $_SESSION["admin"];
                    $userName = $currentUser["name"];
                    echo "Here is ${userName}";
                  }

                ?>
                </span>
              </div>
          </div>
          <nav>
            <ul>
              <li><a href = "/web/tests/index/admin">Tests</a></li>
              <li><a href = "/web/hobby/index/admin">Hobbies</a></li>
              <li><a href = "/web/gallery/index/admin">My Photo</a></li>
              <li><a href = "/web/statistics/index/admin">Statistics</a></li>
              <li><a href = "/web/guestbook/index/admin">Guest Book</a></li>
              <li><a href = "/web/blog/index/admin">Blog</a></li>
              <li><a href = "/web/blogeditor/index/admin">Blog Editor</a></li>
              <li><a href = "/web/contacts/index/admin">Contacts</a></li>
              <li><a href = "/web/about/index/admin">About me</a></li>
              <li><a href = "/web/page/index/admin">Home</a></li>
            </ul>
          </nav>
        </div>
        <div id="header-description">
          <h2>My honor is my first priority...</h2>
          <h1>Welcome, Guys!</h1>
          <div class="spacer">
            <img src="/web/images/spacer.png" alt="spacer">
          </div>
          <div class="text-limiter">
            <p>
              Greeting you, pretty boys and beauties!
              My name is Paul Polbitsev. I'm web developer and there is my
              first lab)
            </p>
          </div>
          <div class="helper-buttons">
            <div class="helper-button">
              <span>Get started now</span>
            </div>
            <div class="helper-button">
              <span>Learn more</span>
            </div>
          </div>
        </div>
        <div id="header-pivot">
          <div id="header-pivot-body">
            <img src="/web/images/pivot.png" alt="piv">
          </div>
        </div>
      </div>
    </header>
    <main>
      <?php include 'application/modes/admin/views/'.$content_view; ?>
    </main>
    <footer>
      <div class="limitation">
        <div id="socials">
          <a href="https://vk.com/misterclass"><img src="/web/images/socicon1.png" alt="vk"></a>
        </div>
        <span>Mister Class &copy All Rights Reserved</span>
      </div>
    </footer>
  </div>
</body>
</html>
