<div class="vertical-limitation">
  <table>
    <caption>Users Statistics</caption>
    <tr>
      <th>ID</th>
      <th>Page</th>
      <th>Date</th>
      <th>Local IP</th>
      <th>Host</th>
      <th>Browser</th>
    </tr>
    <?php

      foreach ($data[0] as $record)
      {
        include "application/modes/admin/views/TableRowStatisticsView.php";
      }

    ?>
  </table>
  <div class="queueFooter">
    <?php

      $pagesCount = $data[1];
      for ($i = 1; $i <= $pagesCount; $i++)
      {
        include "application/modes/user/views/PageLinkView.php";
      }

     ?>
  </div>
</div>
