<div id="form">
  <div class="limitation">
    <div class="form-header">
      <h2>Solve the test!</h2>
      <p>Please, check your knowledge in math)</p>
      <div class="spacer">
        <img src="/web/images/spacer.png" alt="spacer">
      </div>
    </div>
    <div class="feedback">
      <div class="feedback-form alone">
        <form action="/web/tests/submit" method = "POST" name = "testForm" id = "testForm">
          <div class="inputBlock">
            <input type="text"
              placeholder = "Name"
              name = "name"
              autocomplete="off"
              class =
                <?php if (!is_null($data[0])) echo "failedInput" ?>
            >
            <p><?php if (!is_null($data[0])) echo $data[0] ?></p>
          </div>
          <div class="inputBlock">
            <span>Группа:</span>
            <select name="courses" id="courses" form = "form" size = 1>
              <optgroup label = "1 курс">
                <option value="11">ИС/б-11о</option>
                <option value="12">ИС/б-12о</option>
                <option value="13">ИС/б-13о</option>
              </optgroup>
              <optgroup label = "2 курс">
                <option value="21">ИС/б-21о</option>
                <option value="22">ИС/б-22о</option>
                <option value="23">ИС/б-23о</option>
              </optgroup>
              <optgroup label = "3 курс">
                <option value="31">ИС/б-31о</option>
                <option value="32">ИС/б-32о</option>
                <option value="33">ИС/б-33о</option>
              </optgroup>
              <optgroup label = "4 курс">
                <option value="41">ИС/б-41о</option>
                <option value="42">ИС/б-42о</option>
                <option value="43">ИС/б-43о</option>
              </optgroup>
            </select>
          </div>
          <div class = "form-spacer">
            <span>Обозначение функции, обратной экспоненте</span>
            <input type="radio" name = "expRadio" value = "ln" form = "testForm"
            class = <?php if (!is_null($data[1])) echo "failedInput" ?> checked> <p>ln</p>

            <input type="radio" name = "expRadio" value = "lg" form = "testForm"
            class = <?php if (!is_null($data[1])) echo "failedInput" ?>> <p>lg</p>
            <p><?php if (!is_null($data[1])) echo $data[1] ?></p>
          </div>
          <div class="form-spacer">
            <span>Какую парадигму программирования зачастую реализуют игровые движки?</span>
            <select name="paradigmSelect" id="paradigmSelect" size = 1 form = "testForm"
            class = <?php if (!is_null($data[2])) echo "failedInput" ?>>
              <option value="function">Функциональное программирование</option>
              <option value="object">Объектно-ориентированное программирование</option>
              <option value="structure">Структурное программирование</option>
            </select>
            <p><?php if (!is_null($data[2])) echo $data[2] ?></p>
          </div>
          <div class = "form-buttons">
            <input type="submit" class = "helper-button" value = "Submit" form = "testForm">
            <input type= "reset" class = "helper-button" value = "Reset">
          </div>
          <span>Кто дизайнер этой шляпы???</span>
        </form>
      </div>
    </div>
  </div>
</div>
