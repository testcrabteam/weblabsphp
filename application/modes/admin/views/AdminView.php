<?php

  class AdminView extends View
  {
    //public $template_view; // здесь можно указать общий вид по умолчанию.

    function Render($content_view, $template_view, $data = null)
    {
      /*
      if(is_array($data)) {
        // преобразуем элементы массива в переменные
        extract($data);
      }
      */

      include 'application/modes/admin/views/'.$template_view;
    }
  }

?>
