<?php

class AdminGuestbookController extends AdminController
{
  public function __construct()
  {
    parent::__construct();
    $this->model = new GuestbookModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $data = $this->model->GetData();
    $this->view->Render("AdminGuestbookView.php", "AdminLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSend()
  {
    $this->model->SaveMessageFromPost();
    $this->ActionIndex();
  }
}

?>
