<?php

  class AdminController extends Controller
  {
    public function __construct()
    {
      $this->Authenticate();
      $this->view = new AdminView();
    }

    public function Authenticate(){
    if (!isset($_SESSION['isAdmin']) || $_SESSION['isAdmin'] == 0) {
      header('Location:/web/getadmin');
      exit;
    }
  }

  }

?>
