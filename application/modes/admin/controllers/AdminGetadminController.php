<?php

class AdminGetadminController extends AdminController
{

  public function __construct()
  {
    $this->view = new AdminView();
    $this->model = new GetadminModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("AdminGetadminView.php", "AdminLayoutView.php");
  }

  /*---------------------------------------------------*/

  private function RenderWithData($data)
  {
    $this->view->Render("AdminGetadminView.php", "AdminLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSubmit()
  {
    $model = $this->model;
    $data = $model->LogIn();              //Get validation data (errors)
    $this->RenderWithData($data);          //Update page
  }
}

?>
