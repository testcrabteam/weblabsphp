<?php

class AdminGalleryController extends AdminController
{

  public function __construct()
  {
    parent::__construct();
    $this->model = new GalleryModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $data = $this->ActionGetImageData();
    $this->view->Render("AdminGalleryView.php", "AdminLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  private function ActionGetImageData()
  {
    $data = $this->model->GetData();
    return $data;
  }
}

?>
