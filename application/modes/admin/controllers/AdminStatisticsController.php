<?php

class AdminStatisticsController extends AdminController
{
  public function __construct($value='')
  {
    parent::__construct();
    $this->model = new StatisticsModel();
  }

  public function ActionIndex()
  {
    $records = $this->model->GetData();
    $data[0] = $records;
    $data[1] = $this->model->GetPagesCount();
    $this->view->Render("AdminStatisticsView.php", "AdminLayoutView.php", $data);
  }
}

?>
