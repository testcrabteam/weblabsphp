<?php

class AdminBlogController extends AdminController
{
  public function __construct()
  {
    parent::__construct();
    $this->model = new BlogModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $posts = $this->model->GetData();
    $postsElements = $posts[0];
    $comments = $posts[1];

    $data[0] = $postsElements;
    $data[1] = $comments;
    $data[2] = $this->model->GetPagesCount();
    $this->view->Render("AdminBlogView.php", "AdminLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionUpdate()
  {
    $result = $this->model->Update();
    echo json_encode($result);
  }
}

?>
