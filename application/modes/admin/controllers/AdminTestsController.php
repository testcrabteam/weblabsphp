<?php

class AdminTestsController extends AdminController
{

  public function __construct()
  {
    parent::__construct();
    $this->model = new TestsModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("AdminTestsView.php", "AdminLayoutView.php");
  }

  /*---------------------------------------------------*/

  private function RenderWithData($data)
  {
    $this->view->Render("AdminTestsView.php", "AdminLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSubmit()
  {
    $model = $this->model;
    $data = $model->Submit();              //Get validation data (errors)
    $this->RenderWithData($data);          //Update page
  }
}

?>
