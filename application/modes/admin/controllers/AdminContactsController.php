<?php

class AdminContactsController extends AdminController
{

  public function __construct()
  {
    parent::__construct();
    $this->model = new ContactsModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("AdminContactsView.php", "AdminLayoutView.php");
  }

  /*---------------------------------------------------*/

  private function RenderWithData($data)
  {
    $this->view->Render("AdminContactsView.php", "AdminLayoutView.php", $data);
  }

  /*---------------------------------------------------*/

  public function ActionSignUp()
  {
    $model = $this->model;
    $data = $model->SignUp();              //Get validation data (errors)
    $this->RenderWithData($data);          //Update page
  }
}

?>
