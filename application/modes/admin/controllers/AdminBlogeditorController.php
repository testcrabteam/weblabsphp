<?php

class AdminBlogeditorController extends AdminController
{

  public function __construct()
  {
    parent::__construct();
    $this->model = new BlogeditorModel();
  }

  /*---------------------------------------------------*/

  public function ActionIndex()
  {
    $this->view->Render("AdminBlogeditorView.php", "AdminLayoutView.php");
  }

  /*---------------------------------------------------*/

  public function ActionSend()
  {
    $this->model->SaveDataFromPost();
    $this->ActionIndex();
  }

  /*---------------------------------------------------*/

  public function ActionSendFile()
  {
    $this->model->SaveDataFromFile();
    $this->ActionIndex();
  }
}

?>
