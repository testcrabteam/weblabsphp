<?php

  require_once "application/models/validator/RuleInfo.php";
  include "application/models/validator/AdminLoginValidation.php";

  class GetadminModel extends Model
  {

    private $validator;

    public function __construct()
    {
      $rules = array
      (
        new RuleInfo("login", array("IsEmpty", "IsLogin")),
        new RuleInfo("password", array("IsEmpty", "IsPassword"))
      );

      $this->validator = new AdminLoginValidation($rules);
    }

    public function LogIn()
    {
      $fields = array
      (
        "login",
        "password"
      );
      $validator = $this->validator;

      //Validation
      $validator->Validate($fields);
      $errors = $validator->GetErrors();

      $detectIsErrorFunc = function($value)
      {
        if (is_null($value)) return true;
        return false;
      };

      //Detect error on bools values
      $detectErrorsArr = array_map($detectIsErrorFunc, $errors);

      if (!in_array(false, $detectErrorsArr))
      {

        $_SESSION["isAdmin"] = 1;

        $admin = array('name' => "MisterClass Admin");
        $_SESSION["admin"] = $admin;

        header('Location:/web/page/index/admin');
      }
      else $_SESSION["isAdmin"] = 0;

      return $errors;
    }
  }

?>
