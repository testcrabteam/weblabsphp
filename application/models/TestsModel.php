<?php

  require_once "application/models/validator/RuleInfo.php";
  include "application/models/validator/CustomFormResultsVerification.php";
  include "application/models/records/TestsActiveRecord.php";

  class TestsModel extends Model
  {

    private $validator;
    //private $record;

    public function __construct()
    {
      $rules = array
      (
        new RuleInfo("name", array("IsEmpty", "IsName")),
        new RuleInfo("expRadio", array("IsLn")),
        new RuleInfo("paradigmSelect", array("IsObject"))
      );

      $this->validator = new CustomFormResultsVerification($rules);
      $this->record = new TestsActiveRecord();
    }

    public function Submit()
    {
      $fields = array
      (
        "name",
        "expRadio",
        "paradigmSelect"
      );
      $validator = $this->validator;

      //Validation
      $validator->Validate($fields);
      $errors = $validator->GetErrors();

      $nameError = "Uncorrect name, enter 3 words cross space!";
      if (!in_array($nameError, $errors))
      {
        $record = $this->record;

        $record->id = $record->FindMaxId() + 1;
        $record->name = $_POST["name"];
        $record->expradio = $_POST["expRadio"];
        $record->paradigm = $_POST["paradigmSelect"];

        $record->Save();
      }

      return $errors;
    }
  }

?>
