<?php

 class ImageData
 {
   public $imgName;
   public $imgText;

   public function __construct($imageName, $description)
   {
     $this->imgName = $imageName;
     $this->imgText = $description;
   }
 }

?>
