<?php

  include "application/models/keys/IdGenerator.php";

  class IdIncrementGenerator extends IdGenerator
  {

    private $lastId;

    public function __construct()
    {
      $this->lastId = 11;
    }

    public function Generate()
    {
      return ++$this->lastId;
    }
  }

?>
