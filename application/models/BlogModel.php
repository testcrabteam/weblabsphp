<?php

  include "application/models/records/BlogActiveRecord.php";
  include "application/models/records/CommentActiveRecord.php";

  class BlogModel extends Model
  {

    private $record;
    private $recordsPerPage;

    public function __construct()
    {
      $this->record = new BlogActiveRecord();
      $this->recordsPerPage = 3;
    }

    /*---------------------------------------------------*/

    public function GetData()
    {
      $data = array();
      $comments = array();

      // получаем номер страницы
      $page= (isset($_GET['page'])  ? ((int)($_GET['page']) - 1) : 0);

      // вычисляем первый операнд для LIMIT
      $start=abs($page*$this->recordsPerPage);

      $data[0] = BlogActiveRecord::FindByLimit($start, $this->recordsPerPage);
      $this->record = new CommentActiveRecord();

      foreach($data[0] as $record)
      {
        $comments = array_merge($comments, CommentActiveRecord::FindByPostId($record->id));
      }

      $data[1] = $comments;
      return $data;
    }

    /*---------------------------------------------------*/

    public function GetPagesCount()
    {
      $count = BlogActiveRecord::GetPagesCount($this->recordsPerPage);
      return $count;
    }

    /*---------------------------------------------------*/

    public function Update()
    {
      $id = $_POST["blogId"];
      $record = BlogActiveRecord::Find($id);

      $record->theme = $_POST["blogName"];
      $record->content = $_POST["blogContent"];

      $record->Save();
      return $record;
    }
  }

?>
