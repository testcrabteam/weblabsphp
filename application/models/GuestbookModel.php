<?php

  include "application/models/filesystem/MessageFile.php";

  class GuestbookModel extends Model
  {

    private $messageFile;

    public function __construct()
    {
      $this->messageFile = new MessageFile();
    }

    /*---------------------------------------------------*/

    public function GetData()
    {
      $messages = $this->messageFile->GetAllMessages();
      return $messages;
    }

    /*---------------------------------------------------*/

    public function SaveMessageFromPost()
    {
      $messageData = array();

      $messageData["data"] = date('d.m.y H:i:s'); //06.04.20 13:45:00
      $messageData["name"] = $_POST["name"];
      $messageData["email"] = $_POST["email"];
      $messageData["messageText"] = $_POST["messageText"];

      $this->messageFile->AddMessageString($messageData);
    }
  }

?>
