<?php

  require_once "application/models/validator/CustomFormValidation.php";

  class CustomFormResultsVerification extends CustomFormValidation
  {
    public function __construct($rulesArr)
    {
      parent::__construct($rulesArr);
    }

    //Logic methods

    protected function IsLn($data)
    {
      return ($data === "ln") ? true : "No! Ln has exponent basis!";
    }

    /*---------------------------------------------------*/

    protected function IsObject($data)
    {
      return ($data === "object") ? true : "No! How you can create games withot objects???";
    }

    //Validation methods

    public function Validate($dataArray)
    {
      parent::Validate($dataArray);
    }

  }

?>
