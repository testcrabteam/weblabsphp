<?php

  require_once "application/models/validator/FormValidation.php";

  class AdminLoginValidation extends FormValidation
  {
    public function __construct($rulesArr)
    {
      parent::__construct($rulesArr);
    }

    //Logic methods

    protected function IsLogin($data)
    {
      return ($data === "MisterClass") ? true : "Are you crazy?";
    }

    /*---------------------------------------------------*/

    protected function IsPassword($data)
    {
      return ($data === "IsTestWebSite") ? true : "Are you kidding me???";
    }

    //Validation methods

    public function Validate($dataArray)
    {
      parent::Validate($dataArray);
    }
  }

?>
