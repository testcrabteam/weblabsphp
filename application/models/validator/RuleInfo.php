<?php

  class RuleInfo
  {
    public $fieldName;
    public $actionNames;

    public function __construct($fieldName, $actionNames)
    {
      $this->fieldName = $fieldName;
      $this->actionNames = $actionNames;
    }
  }

?>
