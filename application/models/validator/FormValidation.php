<?php

  class FormValidation
  {
    private $rules;
    private $errors;

    public function __construct($rulesArr)
    {
      $this->rules = $rulesArr;
      $this->errors = array();
    }

    //Logic methods

    private function IsEmpty($data)
    {
      return (is_null($data)) ? "Data is empry!" : true;
    }

    /*---------------------------------------------------*/

    private function IsName($data)
    {
      $pattern = '/\w*\s\w*\s\w*/'; //"Begin content end"

      if (preg_match($pattern, $data) == 0)
        return "Uncorrect name, enter 3 words cross space!";

      return true;
    }

    /*---------------------------------------------------*/

    private function IsPhoneNumber($data)
    {
      $pattern = '/^\+(7|3)\d{9,11}$/'; //+7XXXXXXXXX(+xx) or +3XXXXXXXXX(+xx)

      if (preg_match($pattern, $data) == 0)
        return "Error! Input format is +7XXXXXXXXX(xx) or +3XXXXXXXXX(xx)";

      return true;
    }

    //Validation methods

    public function Validate($dataArray)
    {
      foreach ($dataArray as $field)
      {
        $rule = $this->GetRuleByField($field);

        if(!is_null($rule)) $this->ValidateFieldByRule($field, $rule);
      }
    }

    /*---------------------------------------------------*/

    private function GetRuleByField($field)
    {
      foreach ($this->rules as $rule)
      {
        $ruleField = $rule->fieldName;
        if ($ruleField === $field) return $rule;
      }
      return null;
    }

    /*---------------------------------------------------*/

    private function ValidateFieldByRule($field, $rule)
    {
      foreach($rule->actionNames as $action)
      {
        $actionResult = $this->$action($_POST[$field]);

        if(is_string($actionResult))
        {
          array_push($this->errors, $actionResult);
          return;
        }
      }

      array_push($this->errors, null);
    }

    /*---------------------------------------------------*/

    public function GetErrors()
    {
      return $this->errors;
    }
  }

?>
