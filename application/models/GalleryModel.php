<?php

  include "application/models/data/ImageData.php";
  include "application/models/records/GalleryActiveRecord.php";

  class GalleryModel extends Model
  {
    private $record;

    public function __construct()
    {
      $this->record = new GalleryActiveRecord();
    }

    /*---------------------------------------------------*/

    public function GetData()
    {
      $data = $this->record->FindAll();
      return $data;
    }
  }

?>
