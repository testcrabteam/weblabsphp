<?php

  include "application/models/filesystem/file.php";

  class MessageFile extends File
  {

    public function __construct()
    {
      $this->fileName = "data/messages.inc";
      $this->mode = "a+";
      parent::__construct();
    }

    /*---------------------------------------------------*/

    public function AddMessageString($messageData)
    {
      $str = "";

      //Add records of type ' key: value;'
      foreach($messageData as $key=>$value)
      {
        $str = $str . " " . $key . " - " . $value . ";";
      }

      $str = $str . "\n";

      fputs($this->file, $str);
    }

    /*---------------------------------------------------*/

    public function GetAllMessages()
    {
      $file = $this->file;
      $messagesArr = array();

      //Go to begin
      fseek($file, 0);

      //Push all file messages
      while(!feof($file))
      {
        $str = htmlentities(fgets($file));
        if (!is_null($str) && strlen($str) > 4)
        {
          //Get message assoc
          $assocMessage = $this->GetOneMessageData($str);

          //Push message
          $messagesArr[] = $assocMessage;
        }
      }

      return $messagesArr;
    }

    /*---------------------------------------------------*/

    private function GetOneMessageData($messageStr)
    {
      list($date,$name,$email,$text) = explode(";", $messageStr);

      $assocMessage = array();
      $assocMessage["date"] = trim(explode("-", $date)[1]);
      $assocMessage["name"] = trim(explode("-", $name)[1]);
      $assocMessage["email"] = trim(explode("-", $email)[1]);
      $assocMessage["text"] = trim(explode("-", $text)[1]);

      return $assocMessage;
    }
  }

?>
