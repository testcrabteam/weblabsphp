<?php

  include "application/models/filesystem/file.php";

  class PostFile extends File
  {

    public function __construct($fileName)
    {
      $this->fileName = "data/".$fileName;
      $this->mode = "r";
      parent::__construct();
    }

    /*---------------------------------------------------*/

    public function GetAllPosts()
    {
      $file = $this->file;
      $messagesArr = array();

      //Go to begin
      fseek($file, 0);

      //Push all file messages
      while(!feof($file))
      {
        $str = htmlentities(fgets($file));
        if (!is_null($str) && strlen($str) > 4)
        {
          //Get message assoc
          $assocMessage = $this->GetOnePostData($str);

          //Push message
          $messagesArr[] = $assocMessage;
        }
      }

      return $messagesArr;
    }

    /*---------------------------------------------------*/

    private function GetOnePostData($messageStr)
    {
      list($title,$message,$date) = explode(",", $messageStr);

      $assocMessage = array();
      $assocMessage["date"] = trim($date);
      $assocMessage["message"] = trim($message);
      $assocMessage["title"] = trim($title);

      return $assocMessage;
    }
  }

?>
