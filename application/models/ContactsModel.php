<?php

  require_once "application/models/validator/RuleInfo.php";
  include "application/models/validator/ContactsFormValidation.php";
  include "application/models/records/ContactsActiveRecord.php";

  class ContactsModel extends Model
  {

    private $validator;
    private $record;

    public function __construct()
    {
      $rules = array
      (
        new RuleInfo("name", array("IsEmpty", "IsName")),
        new RuleInfo("email", array("IsEmpty")),
        new RuleInfo("phone", array("IsEmpty", "IsPhoneNumber"))
      );

      $this->validator = new ContactsFormValidation($rules);
      $this->record = new ContactsActiveRecord();
    }

    public function SignUp()
    {
      $fields = array
      (
        "name",
        "email",
        "phone"
      );
      $validator = $this->validator;

      //Validation
      $validator->Validate($fields);
      $errors = $validator->GetErrors();


      $detectIsErrorFunc = function($value)
      {
        if (is_null($value)) return true;
        return false;
      };

      //Detect error on bools values
      $detectErrorsArr = array_map($detectIsErrorFunc, $errors);

      if (!in_array(false, $detectErrorsArr))
      {
        $record = $this->record;

        //Init fields
        $record->email = $_POST["email"];
        $record->name = $_POST["name"];
        $record->phone = $_POST["phone"];

        $record->Save();
      }

      return $errors;
    }
  }

?>
