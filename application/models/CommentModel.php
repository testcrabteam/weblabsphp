<?php

  include "application/models/records/CommentActiveRecord.php";

  class CommentModel extends Model
  {

    private $record;

    public function __construct()
    {
      $this->record = new CommentActiveRecord();
    }

    /*---------------------------------------------------*/

    public function GetData()
    {

    }

    /*---------------------------------------------------*/

    public function SaveComment()
    {
      $record = $this->record;
      $currentUser = $_SESSION["user"];

      //Init fields
      $record->id = CommentActiveRecord::FindMaxId() + 1;
      $record->name = $currentUser->name;
      $record->postid = $_POST["postId"];
      $record->date = date('d.m.y H:i:s'); //06.04.20 13:45:00
      $record->text = $_POST["commentText"];

      $record->Save();

      return $record;
    }

  }

?>
