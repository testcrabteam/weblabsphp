<?php
  require_once "application/models/records/UsersActiveRecord.php";

  class LoginModel extends Model
  {
    private $record;

    public function __construct()
    {
      $this->record = new UsersActiveRecord();
    }

    /*---------------------------------------------------*/

    public function LogIn()
    {
      $findError = "Error! We can't find you! Maybe, you'll sign up?";

      $currentUser = $this->record;
      $currentUser->email = $_POST["email"];
      $currentUser->password = $_POST["password"];

      $systemUser = UsersActiveRecord::Find($currentUser->email);

      if ($systemUser) $error = $this->GetLoginError($systemUser, $currentUser);
      else $error = $findError;

      if (is_null($error)) $systemUser->SaveUserInSession();

      return $error;
    }

    /*---------------------------------------------------*/

    private function GetLoginError($systemUser, $currentUser)
    {
      $loginError = "Error! Your login or password are incorrect!";
      if ($systemUser->password !== $currentUser->password) return $loginError;
      return NULL;
    }


  }

?>
