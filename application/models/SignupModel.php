<?php

  require_once "application/models/records/UsersActiveRecord.php";

  class SignupModel extends Model
  {
    public function __construct()
    {
      $this->record = new UsersActiveRecord();
    }

    /*---------------------------------------------------*/

    public function SignUp()
    {
      $record = $this->record;

      //Init fields
      $record->email = $_POST["email"];
      $record->name = $_POST["name"];
      $record->password = $_POST["password"];

      $signUpError = NULL;
      if ($this->IsUserInSytem($record))
      {
        $signUpError = "Error! You are already in system!";
      }
      else $record->Save();

      return $signUpError;
    }

    /*---------------------------------------------------*/

    public function CheckUser()
    {
      $record = $this->record;

      //Init fields
      $record->email = $_GET["email"];
      $signUpError = "Success!";

      if ($record->email == "")
      {
        $signUpError = "Error! Empty!";
      }
      else if ($this->IsUserInSytem($record))
      {
        $signUpError = "Error! You are already in system!";
      }

      return $signUpError;
    }

    /*---------------------------------------------------*/

    private function IsUserInSytem($currentUser)
    {
      $systemUser = UsersActiveRecord::Find($currentUser->email);

      if ($systemUser) return true;
      return false;
    }
  }

?>
