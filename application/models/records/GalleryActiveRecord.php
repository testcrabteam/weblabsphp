<?php

  class GalleryActiveRecord extends BaseActiveRecord
  {
    public $id;
    public $name;
    public $title;

    public function __construct()
    {
      static::$tableName = "gallery";
      static::$idName = "id";
      parent::__construct();
    }
  }


?>
