<?php

  class CommentActiveRecord extends BaseActiveRecord
  {
    public $id;
    public $name;
    public $postid;
    public $date;
    public $text;

    public function __construct()
    {
      static::$tableName = "comments";
      static::$idName = "id";
      parent::__construct();
    }

    /*---------------------------------------------------*/

    public function FindMaxId()
    {
      $sql = "SELECT MAX(id) AS maxId FROM ".static::$tableName." WHERE 1";
      $stmt = static::$pdo->query($sql);

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      if (!$row) return null;

      $maxId = $row["maxId"];

      return $maxId;

    }

    /*---------------------------------------------------*/

    public static function FindByPostId($postId)
    {
      $postId = static::ConvertFieldValueToString("postid", $postId);
      $sql = "SELECT * FROM ".static::$tableName." WHERE postid = ${postId}";
      $stmt = static::$pdo->query($sql);

      $resultArr = array();

      //Save table records
      while ($row = $stmt->fetch())
      {
        $ar_obj = new static();

        //Save current table record
        foreach ($row as $key => $value)
        {
          $ar_obj ->$key = $value;
        }

        array_push($resultArr, $ar_obj);
      }

      return $resultArr;
    }
    //
    // /*---------------------------------------------------*/
    //
    // public function GetPagesCount($recordsPerPage)
    // {
    //   // выводим ссылки на страницы:
    //   $query = "SELECT * FROM ".static::$tableName;
    //   $totalRows = count(static::$pdo->query($query)->fetchAll());
    //
    //   // Определяем общее количество страниц
    //   $numPages = ceil($totalRows/$recordsPerPage);
    //
    //   return $numPages;
    // }
  }


?>
