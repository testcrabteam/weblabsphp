<?php

  class UsersActiveRecord extends BaseActiveRecord
  {
    public $email;
    public $name;
    public $password;

    public function __construct()
    {
      static::$tableName = "users";
      static::$idName = "email";
      parent::__construct();
    }

    /*---------------------------------------------------*/

    public function Save()
    {
      $this->SaveUserInSession();
      parent::Save();
    }

    /*---------------------------------------------------*/

    public function SaveUserInSession()
    {
      //session_start();
      $_SESSION["user"] = $this;
    }
  }


?>
