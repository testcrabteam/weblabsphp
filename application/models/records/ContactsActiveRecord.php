<?php

  class ContactsActiveRecord extends BaseActiveRecord
  {
    public $email;
    public $name;
    public $phone;

    public function __construct()
    {
      static::$tableName = "contacts";
      static::$idName = "email";
      parent::__construct();
    }
  }


?>
