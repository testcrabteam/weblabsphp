<?php

  class TestsActiveRecord extends BaseActiveRecord
  {
    public $id;
    public $name;
    public $expradio;
    public $paradigm;

    public function __construct()
    {
      static::$tableName = "tests";
      static::$idName = "id";
      parent::__construct();
    }

    /*---------------------------------------------------*/

    public function FindMaxId()
    {
      $sql = "SELECT MAX(id) AS maxId FROM ".static::$tableName." WHERE 1";
      $stmt = static::$pdo->query($sql);

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      if (!$row) return null;

      $maxId = $row["maxId"];

      return $maxId;

    }
  }


?>
