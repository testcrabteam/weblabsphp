<?php

  class BlogActiveRecord extends BaseActiveRecord
  {
    public $id;
    public $date;
    public $imagename;
    public $theme;
    public $content;

    public function __construct()
    {
      static::$tableName = "blog";
      static::$idName = "id";
      parent::__construct();
    }

    /*---------------------------------------------------*/

    public function FindMaxId()
    {
      $sql = "SELECT MAX(id) AS maxId FROM ".static::$tableName." WHERE 1";
      $stmt = static::$pdo->query($sql);

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      if (!$row) return null;

      $maxId = $row["maxId"];

      return $maxId;

    }

    /*---------------------------------------------------*/

    public static function FindByLimit($start, $end)
    {
      $sql = "SELECT * FROM ".static::$tableName." ORDER BY id LIMIT $start, $end";
      $stmt = static::$pdo->query($sql);

      $resultArr = array();

      //Save table records
      while ($row = $stmt->fetch())
      {
        $ar_obj = new static();

        //Save current table record
        foreach ($row as $key => $value)
        {
          $ar_obj ->$key = $value;
        }

        array_push($resultArr, $ar_obj);
      }

      return $resultArr;
    }

    /*---------------------------------------------------*/

    public function GetPagesCount($recordsPerPage)
    {
      // выводим ссылки на страницы:
      $query = "SELECT * FROM ".static::$tableName;
      $totalRows = count(static::$pdo->query($query)->fetchAll());

      // Определяем общее количество страниц
      $numPages = ceil($totalRows/$recordsPerPage);

      return $numPages;
    }
  }


?>
