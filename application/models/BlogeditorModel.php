<?php

  include "application/models/records/BlogActiveRecord.php";
  include "application/models/filesystem/PostFile.php";

  class BlogeditorModel extends Model
  {

    private $record;

    public function __construct()
    {
      $this->record = new BlogActiveRecord();
    }

    /*---------------------------------------------------*/

    public function GetData()
    {

    }

    /*---------------------------------------------------*/

    public function SaveDataFromPost()
    {
      $this->SaveDataInBaseFromPost();
      if (isset($_FILES["imageFile"])) $this->SaveDataImageToServer();
    }

    /*---------------------------------------------------*/

    private function SaveDataInBaseFromPost()
    {
      $this->record->id = $this->record->FindMaxId() + 1;
      $this->record->date = date('Y-m-d H:i:s'); //20-04-07 10:37:00
      $this->record->theme = $_POST["theme"];
      $this->record->content = $_POST["content"];

      $this->record->imagename = (isset($_FILES["imageFile"]))
        ? $_FILES["imageFile"]["name"]
        : NULL;

      $this->record->Save();
    }

    /*---------------------------------------------------*/

    private function SaveDataImageToServer()
    {
      $source = $_FILES["imageFile"]["tmp_name"];
      $dest = "images/user/".$_FILES["imageFile"]["name"];
      move_uploaded_file($source, $dest);
    }

    /*---------------------------------------------------*/

    public function SaveDataFromFile()
    {
      if(isset($_FILES["postFile"]))
      {
        $this->SaveDataFileToServer("postFile");
        $file = new PostFile($_FILES["postFile"]["name"]);
        $posts = $file->GetAllPosts();

        foreach ($posts as $post)
        {
          $this->record->id = $this->record->FindMaxId() + 1;
          $this->record->date = $post["date"];
          $this->record->theme = $post["title"];
          $this->record->content = $post["message"];

          $this->record->imagename = NULL;
          $this->record->Save();
        }
      }
    }

    /*---------------------------------------------------*/

    private function SaveDataFileToServer($fileName)
    {
      $source = $_FILES[$fileName]["tmp_name"];
      $dest = "data/".$_FILES[$fileName]["name"];
      move_uploaded_file($source, $dest);
    }
  }

?>
