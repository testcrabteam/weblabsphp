<?php

  require_once "application/models/records/StatisticsActiveRecord.php";

  class StatisticsModel extends Model
  {

    private $record;
    private $recordsPerPage;

    public function __construct()
    {
      $this->recordsPerPage = 10;
      $this->record = new StatisticsActiveRecord();
    }

    /*---------------------------------------------------*/

    public function SaveStatistics($page)
    {
      $statistics = $this->record;

      $statistics->id = $statistics->FindMaxId() + 1;
      $statistics->page = $page;
      $statistics->date = date('Y-m-d h:m:s');
      $statistics->address = $_SERVER["REMOTE_ADDR"];
      $statistics->host = gethostbyaddr($statistics->address);
      $statistics->browser = $_SERVER["HTTP_USER_AGENT"];

      $statistics->Save();
    }

    /*---------------------------------------------------*/

    public function GetData()
    {
      // получаем номер страницы
      $page= (isset($_GET['page'])  ? ((int)($_GET['page']) - 1) : 0);

      // вычисляем первый операнд для LIMIT
      $start=abs($page*$this->recordsPerPage);

      $data = StatisticsActiveRecord::FindByLimit($start, $this->recordsPerPage);
      return $data;
    }

    /*---------------------------------------------------*/

    public function GetPagesCount()
    {
      $count = StatisticsActiveRecord::GetPagesCount($this->recordsPerPage);
      return $count;
    }
  }

?>
